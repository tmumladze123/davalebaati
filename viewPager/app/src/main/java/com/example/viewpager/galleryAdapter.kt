package com.example.viewpager

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class galleryAdapter (private val context: Context): RecyclerView.Adapter<galleryAdapter.ViewHolder>() {
    var name= mutableListOf<String>("a", "b" , "c","d","e","f","g","h","i","j")
    var desc= mutableListOf<String>("d", "e" , "f","d","e","f","g","h","i","j")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): galleryAdapter.ViewHolder {
       return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_page, parent, false))
    }

    override fun onBindViewHolder(holder: galleryAdapter.ViewHolder, position: Int) {
      holder.Amount.text=name.size.toString()
      holder.Name.text=name[position]
        holder.Desc.text=desc[position]
    }

    override fun getItemCount(): Int {
        return  name.size
    }

    inner class ViewHolder(itemView : View): RecyclerView.ViewHolder(itemView){
        var Name : TextView=itemView.findViewById(R.id.tvName)
        var Amount : TextView=itemView.findViewById(R.id.tvAmount)
        var Desc : TextView=itemView.findViewById(R.id.tvDesc)



    }
}
