package com.example.viewpager

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity(){
    var mambo: String.()->Unit={

    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        createAdapter()

    }

    fun createAdapter()
    {   val pager=findViewById<ViewPager2>(R.id.viewPager)
        val tabLayout=findViewById<TabLayout>(R.id.tabLayout)
        //pager.adapter = galleryAdapter(this)
        pager.adapter = galleryAdapter(this)
        pager.orientation = ViewPager2.ORIENTATION_VERTICAL
        TabLayoutMediator(tabLayout, pager) {tab, position ->
            tab.text = "${position + 1}"
        }.attach()


        }

}
